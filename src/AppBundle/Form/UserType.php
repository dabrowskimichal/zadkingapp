<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;

class UserType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $countries = Intl::getRegionBundle()->getCountryNames();
        $countries2 = array_flip($countries);
        $builder
                ->add('name')
                ->add('surName')
                ->add('city')
                ->add('country', ChoiceType::class, array(
                    'choices' => $countries2,
                    'preferred_choices' => array('PL'),
                ))
                ->add('availability', ChoiceType::class, array(
                    'choices' => array(
                        'praca zdalna' => 'praca zdalna',
                        'praca na miejscu' => 'praca na miejscu'
                    ),
                    'expanded' => true,
                    'multiple' => true
                ))
                ->add('profession', ChoiceType::class, array(
                    'choices' => array(
                        'Programista' => 'Programista',
                        'Koder' => 'Koder',
                        'Designer' => 'Designer',
                    ),
                ))
                ->add('experience');
        
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_user';
    }

}
